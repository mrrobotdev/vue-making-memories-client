import api from '../api/index.js'

export const getPosts = async () => {
    try {
        const { data } = await api.fetchPosts();

        return data
    } catch (error) {
        console.log(error.message);
    }
};

export const createPost = async (post) => {
    try {
        const { data } = await api.createPost(post);

        return data
    } catch (error) {
        console.log(error.message);
    }
};

export const updatePost = async (id, post) => {
    try {
        const { data } = await api.updatePost(id, post);

        return data
    } catch (error) {
        console.log(error.message);
    }
};

export const likePost = async (id) => {
    try {
        const { data } = await api.likePost(id);

        return data
    } catch (error) {
        console.log(error.message);
    }
};

export const deletePost = async (id) => {
    try {
        await api.deletePost(id);

    } catch (error) {
        console.log(error.message);
    }
};